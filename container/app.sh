#!/bin/bash

# Команда для миграции базы данных
python3 manage.py migrate

# Команда для запуска приложения
python3 manage.py runserver 0.0.0.0:8000
